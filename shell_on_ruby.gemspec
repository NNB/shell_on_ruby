# frozen_string_literal: true

require_relative 'lib/ruby_on_shell/version'

Gem::Specification.new do |spec|
  spec.name = 'shell_on_ruby'
  spec.version = ShellOnRuby::VERSION
  spec.authors = ['NNB']
  spec.email = ['nnbnh@protonmail.com']

  spec.summary = "Convenient subprocess interface."
  spec.description = "Shell on Ruby is a convenient subprocess interface that allows you to call any program as if it were a method."
  spec.homepage = 'https://codeberg.org/NNB/shell_on_ruby.rb'
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 3.0'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['changelog_uri'] = "#{spec.homepage}/src/branch/main/CHANGELOG.md"

  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.extra_rdoc_files = ['README.md', 'CHANGELOG.md', 'LICENSE.txt']
  spec.require_paths = ['lib']
end
