# frozen_string_literal: true

require_relative 'sh/extensions'
require_relative 'pipe/extensions'

include ShellOnRuby
