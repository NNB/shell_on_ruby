# frozen_string_literal: true

require_relative 'command' unless defined? ShellOnRuby::Command

module ShellOnRuby
  # A command shell.
  class Sh
    # A standard input for a shell command.
    class Input
      attr_reader :string

      alias to_s string

      def initialize(string)
        @string = string.to_s
      end

      def method_missing(name, *args, **kargs)
        Sh.new(ShellOnRuby::Command.new(name, *args, **kargs), stdin: @string).stdout
      end
    end

    attr_reader :command, :stdin, :stdout

    alias to_s stdout

    def initialize(command, stdin: nil)
      raise TypeError, "no implicit conversion of #{command.class} into ShellOnRuby::Command" unless command.is_a?(ShellOnRuby::Command)

      @command = command
      @stdin = stdin

      IO.popen([@command.name, *@command.arguments, @command.options], 'r+') do |pipe|
        pipe.write @stdin.to_s if @stdin
        pipe.close_write

        @stdout = pipe.read
      end
    end

    def self.method_missing(name, *args, **kargs)
      new(ShellOnRuby::Command.new(name, *args, **kargs)).stdout
    end
  end

  class Command
    def stdout
      ShellOnRuby::Sh.new(self).stdout
    end
  end
end
