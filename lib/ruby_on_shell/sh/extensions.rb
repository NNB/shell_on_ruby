# frozen_string_literal: true

require_relative '../sh' unless defined? ShellOnRuby::Sh

def self.method_missing(name, *args, **kargs)
  ShellOnRuby::Sh.new(ShellOnRuby::Command.new(name, *args, **kargs)).stdout
end

class String
  # Returns a standard input of `self` for a shell command.
  def sh
    ShellOnRuby::Sh::Input.new self
  end
end
