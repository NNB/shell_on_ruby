# frozen_string_literal: true

require_relative '../pipe' unless defined? ShellOnRuby::Pipe

class String
  # Returns a standard input of `self` for a pipeline.
  def pipe
    ShellOnRuby::Pipe::Input.new self
  end
end
