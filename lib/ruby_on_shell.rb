# frozen_string_literal: true

require_relative 'ruby_on_shell/version'
require_relative 'ruby_on_shell/sh'
require_relative 'ruby_on_shell/pipe'

# Convenient subprocess interface.
module ShellOnRuby
end
